jogadas=[
	[1,2,3],
	[1,4,7],
	[1,5,9],
	[2,5,8],
	[3,5,7],
	[3,6,9],
	[4,5,6],
	[7,8,9],
];
contador=0;
jogador=1;
fim=false;
vng=0;
thn=0;

function jogar(bloco){
	fundo=document.getElementById(bloco);
	if(contador!=9 && fim==false){
		if((fundo.style.backgroundImage=="")){
			if(jogador==1){
				fundo.style.backgroundImage="url(midia/vng.jpg)";
			}else{
				fundo.style.backgroundImage="url(midia/thn.jpeg)";
			}
			contador+=1;
			checar();
		}
	}
}

function checar(){
	for(var i=0; i<8; i++){
		bloco1=document.getElementById(jogadas[i][0]).style.backgroundImage;
		bloco2=document.getElementById(jogadas[i][1]).style.backgroundImage;
		bloco3=document.getElementById(jogadas[i][2]).style.backgroundImage;
		if(bloco1==bloco2 && bloco2==bloco3 && bloco1!==""){
			if(jogador==1){
				document.getElementById("mensagem").innerHTML="Os Vingadores derrotaram Thanos!";
				document.getElementById("reset").innerHTML="<button class='btn btn-danger' onclick='resetar()'>Batalhar novamente</button>";
				vng+=1;
			}else{
				document.getElementById("mensagem").innerHTML="Thanos exterminou metade do Universo!";
				document.getElementById("reset").innerHTML="<button class='btn btn-danger' onclick='resetar()'>Batalhar novamente</button>";
				thn+=1;
			}
			document.getElementById('placar').innerHTML="<span>VNG "+vng+" X "+thn+" THN</span>";
			fim=true;
		}
	}
	if(fim==false){
		if(contador==9){
			document.getElementById("mensagem").innerHTML="Deu velha!";
			document.getElementById("reset").innerHTML="<button class='btn btn-danger' onclick='resetar()'>Batalhar novamente</button>";
		}else{
			if(jogador==1){
				jogador=2;
				document.getElementById("mensagem").innerHTML="Vez dos Thanos";
			}else{
				jogador=1;
				document.getElementById("mensagem").innerHTML="Vez dos Vingadores";
			}
		}		
	}

}

function resetar(){
	document.getElementById("mensagem").innerHTML="Os heróis começam";
	document.getElementById("reset").innerHTML="";
	jogador=1;
	contador=0;
	fim=false;
	for(var i=1; i<10; i++){
		document.getElementById(i).style.backgroundImage="";
	}
}